# Qweex MainWindow #

This is a very simple custom class that extends QMainWindow for Qt 5.

Its purposes are:

 - To handle most of the cross-platform junk that is the same for each project
 - To handle creating an "About" window & "License" dialog
 - To handle some payment/begging stuffs, license keys and whatnot.

The last bullet isn't checked in to this repo, but it should still function just fine!
I purposefully was careful to make sure that everything would compile without it AND that nothing in the program would be weird.


## License & Use ##

**To anyone other than me **: You can feel free to use this class to your hearts content, fork it, whatever.
It is BSD licensed. But there's a reason I named it _Qweex_ MainWindow. It's specific to what I want. Just keep that in mind.

And no, I don't ever plan to release the 'purchase' subfolder, though I have included a section towards the end of this readme on implementing your own.


## Using in a project ##

### 1. Include the pri file ###

Assuming you have already cloned this repo into a subdirectory of your project ("qwmainwindow", for example), add

    include(qwmainwindow/qweex.pri)

to your PRO file. That's it.


### 2. Customize for the project at hand ###

Copy `project.h.sample` to your project's base directory & edit it to your liking.


## 3. Subclass Qweex::MainWindow ###

When you are creating your own MainWindow class, subclass Qweex::MainWindow instead of QMainWindow.

## 4. Use the functions, Luke ###

 - `doPlatformSpecific()` = Does platform specific things, like adding the "Window" menu to Mac.
 - `addMenus()` = Adds the menu items for "About" and "License", in addition to the uber-secret purchase stuff, if available.

Now both are optional, but if you do call them, call them _after you've added all your menu items_.

To make it more clear, if you do:

    doPlatformSpecific();
    addMenus();
    menuBar()->addMenu(new QMenu("File"));
    ...

On a Mac, your menu will end out looking like: `Window | Help | File`

The correct way, then would be:

    menuBar()->addMenu(new QMenu("File"));
    ..
    doPlatformSpecific();
    addMenus();

which will produce: `File | Window | Help`


## Implementing Purchase ##

I've supplied a very simple `purchase.h.sample` file that holds the minimum of what you need. You can add whatever logic you want behind closed doors as long as you have those functions & members.

Oh, you can optionally listen for the `receivedLicenseConfirmation()` signal for your MainWindow, if you want to do anything more than just update the menus.
