#ifndef QWEEX_MAIN_WINDOW_H
#define QWEEX_MAIN_WINDOW_H

#include "about_window.h"

#ifdef SUPPORT_THE_DEV
#include "purchase/purchase.h"
#endif

#include <QMainWindow>
#include <QMenu>
#include <QAction>
#include <QDesktopServices>
#include <QMenuBar>

namespace Qweex {

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);

protected:
    void doPlatformSpecific();
    void addMenus();

public slots:
    #ifdef SUPPORT_THE_DEV
    void buy()
    {
        QDesktopServices::openUrl(QUrl(Qweex::Purchase::BUY_URL));
    }
    void showLicense()
    {
        Qweex::Purchase::showLicenseInfo(this);
    }
    void enterLicense()
    {
        Qweex::Purchase::enterLicenseInfo(this);
    }
    void receivedLicenseConfirmation();
    #endif

private:
    AboutWindow* about_window;
    bool done_platform_specific = false, added_menus = false;
    QMenu* Help;

private slots:
    void onZoomMenuItem() {
        if(this->isMaximized())
            showNormal();
        else
            showMaximized();
    }
};

}
#endif
